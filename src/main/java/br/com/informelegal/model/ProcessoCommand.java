package br.com.informelegal.model;

import java.util.List;

import lombok.Data;

@Data
public class ProcessoCommand {
	private TribunalCommand tribunal;
	private String opcaoPesquisa;
	private String valorConsulta;
	private List<String> notificationTokens;
}
