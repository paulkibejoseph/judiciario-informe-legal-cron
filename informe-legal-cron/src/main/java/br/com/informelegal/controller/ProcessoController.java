package br.com.informelegal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.informelegal.model.ProcessoCommand;
import br.com.informelegal.model.ProcessoQuery;
import br.com.informelegal.service.ProcessoService;

@RestController
public class ProcessoController {

	@Autowired
	private ProcessoService processoService;

	@RequestMapping(value = "/tje/tjsp/processo", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<List<ProcessoQuery>> buscar(@RequestHeader String apiKey,
			@RequestBody ProcessoCommand command) {
		return processoService.buscarProcessosTJSP(apiKey, command);
	}
}
