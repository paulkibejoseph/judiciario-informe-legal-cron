package br.com.informelegal.service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import br.com.informelegal.model.ProcessoCommand;
import br.com.informelegal.model.ProcessoQuery;
import br.com.informelegal.model.TribunalCommand;

@Service
public class ProcessoService {
	private static final Log LOGGER = LogFactory.getLog(ProcessoService.class);

	private static final String KEY_PATH = "src/main/resources/serviceAccountKey.json";
	private static final String DB_URL = "https://informe-legal.firebaseio.com";

	private static final String KEY_TRIBUNAL = "tribunais";
	private static final String KEY_TRIBUNAL_TJE = "tribunal-justica-estadual";
	private static final String KEY_TRIBUNAL_TJSP = "sp";
	private static final String KEY_PROCESSOS = "processos";
	private static final String KEY_NUMPROC = "NUMPROC";
	private static final String API_TJSP = "https://us-central1-informe-legal.cloudfunctions.net/consultarProcesso/";

	@Autowired
	private RestTemplate restTemplate;

	private Firestore firestore;

	public ProcessoService() {
		try {
			FileInputStream serviceAccount = new FileInputStream(KEY_PATH);
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount)).setDatabaseUrl(DB_URL).build();
			FirebaseApp.initializeApp(options);
			this.firestore = FirestoreClient.getFirestore();
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	public List<ProcessoQuery> acompanhamentoDeProcessosTJSP() {
		List<ProcessoQuery> processos = new ArrayList<>();
		try {
			if (this.firestore != null) {
				ApiFuture<QuerySnapshot> apiFuture = this.firestore.collection(KEY_TRIBUNAL).document(KEY_TRIBUNAL_TJE)
						.collection(KEY_PROCESSOS).get();
				List<QueryDocumentSnapshot> documents = apiFuture.get().getDocuments();
				documents.parallelStream().forEach(document -> {
					if (document.exists()) {
						Object response = consultarProcessoTJSP("paul-joseph", document.getId(),
								Arrays.asList("DeviceToken1", "DeviceToken2"));
						LOGGER.info(response);
						ProcessoQuery processoQuery = new ProcessoQuery();
						processoQuery.setNumProcesso(document.getId());
						processos.add(processoQuery);
					} else {
						LOGGER.info("Nenhum registro foi encontrado.");
					}
				});
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return processos;
	}

	public List<ProcessoQuery> acompanhamentoDeProcessosTJSP(String apiKey, ProcessoCommand command) {
		List<ProcessoQuery> processos = new ArrayList<>();
		try {
			if (this.firestore != null && command != null && !StringUtils.isEmpty(command.getValorConsulta())) {
				ApiFuture<DocumentSnapshot> apiFuture = this.firestore.collection(KEY_TRIBUNAL)
						.document(KEY_TRIBUNAL_TJE).collection(KEY_PROCESSOS).document(command.getValorConsulta())
						.get();
				DocumentSnapshot document = apiFuture.get();
				if (document.exists()) {
					Object response = consultarProcessoTJSP(apiKey, document.getId(),
							Arrays.asList("DeviceToken1", "DeviceToken2"));
					LOGGER.info(response);
					ProcessoQuery processoQuery = new ProcessoQuery();
					processoQuery.setNumProcesso(document.getId());
					processos.add(processoQuery);
				} else {
					LOGGER.info("Nenhum registro foi encontrado.");
				}
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return processos;
	}

	public ResponseEntity<List<ProcessoQuery>> buscarProcessosTJSP(String apiKey, ProcessoCommand command) {
		List<ProcessoQuery> processos = acompanhamentoDeProcessosTJSP(apiKey, command);
		return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON_UTF8).body(processos);
	}

	private Object consultarProcessoTJSP(String apiKey, String numProcesso, List<String> tokens) {
		TribunalCommand tribunalCommand = new TribunalCommand();
		tribunalCommand.setKey(KEY_TRIBUNAL_TJE);
		tribunalCommand.setSecao(KEY_TRIBUNAL_TJSP);
		ProcessoCommand processoCommand = new ProcessoCommand();
		processoCommand.setTribunal(tribunalCommand);
		processoCommand.setOpcaoPesquisa(KEY_NUMPROC);
		processoCommand.setValorConsulta(numProcesso);
		processoCommand.setNotificationTokens(tokens);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", "application/json");
		httpHeaders.set("x-api-key", apiKey);
		HttpEntity<ProcessoCommand> request = new HttpEntity<>(processoCommand, httpHeaders);
		Object response = restTemplate.postForObject(API_TJSP, request, Object.class);
		return response;
	}

}
