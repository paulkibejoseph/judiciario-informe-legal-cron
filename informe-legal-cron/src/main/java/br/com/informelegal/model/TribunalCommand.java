package br.com.informelegal.model;

import lombok.Data;

@Data
public class TribunalCommand {
	// "$key"
	private String key;
	private String secao;
}
