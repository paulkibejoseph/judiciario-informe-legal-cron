package br.com.informelegal.utils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.informelegal.model.ProcessoQuery;
import br.com.informelegal.service.ProcessoService;

@Component
@EnableScheduling
public class Scheduler {
	private static final Log LOGGER = LogFactory.getLog(Scheduler.class);

	@Autowired
	private ProcessoService processoService;

	private static final String TIME_ZONE = "America/Sao_Paulo";
	private final long SEGUNDO = 1000;
	private final long MINUTO = SEGUNDO * 60;
	private final long HORA = MINUTO * 60;

	@Scheduled(fixedDelay = SEGUNDO * 30, zone = TIME_ZONE)
	public void acompanhamentoDeProcessos_TJE_TJSP() {
		List<ProcessoQuery> processos = processoService.acompanhamentoDeProcessosTJSP();
		LOGGER.info("Acompanhamento de Processos de TJE-TJSP [ " + LocalDateTime.now() + " ]");
		LOGGER.info(processos.stream().map(processo -> processo.getNumProcesso()).collect(Collectors.toList()));
	}
}
